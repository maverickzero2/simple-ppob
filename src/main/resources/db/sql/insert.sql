INSERT INTO T_MST_SUPPLIER VALUES (1, 'S001', 'SUPPLIER 1', 'SUPPLIER 1');
INSERT INTO T_MST_SUPPLIER VALUES (2, 'S002', 'SUPPLIER 2', 'SUPPLIER 2');

INSERT INTO T_MST_PRODUCT (CODE, NAME, PRICE, DESCRIPTION, TOKEN, SUPPLIER_ID)
VALUES ('E001', 'ELECTRICITY', 1750, 'Prepaid Electricity PLN Indonesia', '163yghjknqhwve61723', 1);
INSERT INTO T_MST_PRODUCT (CODE, NAME, PRICE, DESCRIPTION, TOKEN, SUPPLIER_ID)
VALUES ('W001', 'WATER', 1500, 'Bill pay Electricity PDAM Indonesia', '17y23ghj1b2g3i12u322', 1);
INSERT INTO T_MST_PRODUCT (CODE, NAME, PRICE, DESCRIPTION, TOKEN, SUPPLIER_ID)
VALUES ('I001', 'INSURANCE', 2500, 'Subscribe payment for specific period of time', '6789oghjaksjdaksdaaa', 1);

INSERT INTO T_MST_PRODUCT (CODE, NAME, PRICE, DESCRIPTION, TOKEN, SUPPLIER_ID)
VALUES ('E001', 'ELECTRICITY', 1850, 'Prepaid Electricity PLN Indonesia', '163yghjknqhwve61723', 2);
INSERT INTO T_MST_PRODUCT (CODE, NAME, PRICE, DESCRIPTION, TOKEN, SUPPLIER_ID)
VALUES ('W001', 'WATER', 1255, 'Bill pay Electricity PDAM Indonesia', '17y23ghj1b2g3i12u322', 2);
INSERT INTO T_MST_PRODUCT (CODE, NAME, PRICE, DESCRIPTION, TOKEN, SUPPLIER_ID)
VALUES ('I001', 'INSURANCE', 2500, 'Subscribe payment for specific period of time', '6789oghjaksjdaksdaaa', 2);