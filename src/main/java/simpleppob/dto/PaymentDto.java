package simpleppob.dto;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PaymentDto {

  @JsonProperty("transactionid")
  private String transactionCode;
  private String email;
  private String supplierCode;
  private String productCode;
  private String token;
  private String state;
  private BigDecimal fee;
  private BigDecimal total;
  @JsonProperty("lastbalance")
  private BigDecimal balance;

}
