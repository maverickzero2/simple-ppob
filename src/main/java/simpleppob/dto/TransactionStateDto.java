package simpleppob.dto;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TransactionStateDto {

  @JsonProperty("transactionid")
  private String transactionCode;
  private String state;
  @JsonProperty("lastbalance")
  private BigDecimal balance;

}
