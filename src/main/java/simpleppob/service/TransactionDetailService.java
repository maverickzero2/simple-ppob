package simpleppob.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import simpleppob.json.InquiryDataRequest;

public interface TransactionDetailService {

  int save(final InquiryDataRequest inquiryData, final String state) throws JsonProcessingException;

}
