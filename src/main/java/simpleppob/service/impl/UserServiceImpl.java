package simpleppob.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import simpleppob.dao.UserDao;
import simpleppob.dto.UserDto;
import simpleppob.helper.CustomResponseHelper;
import simpleppob.json.SaveUserRequest;
import simpleppob.service.JsonValidate;
import simpleppob.service.UserService;

@Service
public class UserServiceImpl implements UserService, JsonValidate {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

  private static final String DUPLICATED_EMAIL = "please use another email.";

  @Autowired
  UserDao userDao;

  @Override
  public ResponseEntity<Object> save(SaveUserRequest userRequest) {
    try {
      validate(userRequest, "/user.json");

      int save = userDao.save(userRequest);

      if (save > 0) {
        return find(userRequest.getEmail());
      } else {
        throw new Exception("failed save user.");
      }
    } catch (IllegalArgumentException i) {
      LOGGER.error(i.getMessage(), i);
      return CustomResponseHelper.create(HttpStatus.BAD_REQUEST, i.getMessage());
    } catch (DuplicateKeyException d) {
      LOGGER.error(d.getMessage(), d);
      return CustomResponseHelper.create(HttpStatus.BAD_REQUEST, DUPLICATED_EMAIL);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  public ResponseEntity<Object> find(String email) {
    try {
      if (null == email) {
        return CustomResponseHelper.create(HttpStatus.BAD_REQUEST);
      }

      UserDto user = userDao.find(email);

      return new ResponseEntity<>(user, HttpStatus.OK);
    } catch (EmptyResultDataAccessException em) {
      LOGGER.error("user not found.", em);
      return CustomResponseHelper.create(HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
