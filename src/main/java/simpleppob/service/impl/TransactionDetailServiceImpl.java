package simpleppob.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import simpleppob.dao.TransactionDetailDao;
import simpleppob.json.InquiryDataRequest;
import simpleppob.service.TransactionDetailService;

@Service
public class TransactionDetailServiceImpl implements TransactionDetailService {

  @Autowired
  TransactionDetailDao transactionDetailDao;

  @Override
  public int save(InquiryDataRequest inquiryData, String state) throws JsonProcessingException {
    return transactionDetailDao.save(inquiryData, state);
  }

}
