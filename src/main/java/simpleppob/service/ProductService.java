package simpleppob.service;

import org.springframework.http.ResponseEntity;

public interface ProductService {

  ResponseEntity<Object> find(String productCode);

}
