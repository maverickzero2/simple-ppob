package simpleppob.service;

import org.springframework.http.ResponseEntity;

public interface RollbackService {

  ResponseEntity<Object> rollback(final String transactionCode);

}
