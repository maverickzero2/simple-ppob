package simpleppob.service;

import simpleppob.dto.InquiryDto;
import simpleppob.dto.ProductDto;
import simpleppob.json.InquiryDataRequest;

public interface TransactionService {

  int save(final InquiryDataRequest inquiryData, final ProductDto product);

  InquiryDto findInquiry(final String transactionCode);

}
