package simpleppob.type;

public enum TransactionState {

  INQUIRY, PAYMENT, COMMIT, ROLLBACK

}
