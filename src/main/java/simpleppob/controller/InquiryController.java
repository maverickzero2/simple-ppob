package simpleppob.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import simpleppob.json.InquiryDataRequest;
import simpleppob.service.InquiryService;

@RestController
public class InquiryController {

  @Autowired
  InquiryService inquiryService;

  @PostMapping(path = "/inquiry", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> inquiry(@RequestBody InquiryDataRequest inquiryRequest) {
    return inquiryService.inquiry(inquiryRequest);
  }

}
