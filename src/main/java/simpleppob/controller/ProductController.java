package simpleppob.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import simpleppob.service.ProductService;

@RestController
public class ProductController {

  @Autowired
  ProductService productService;

  @GetMapping(path = "/product/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getProductByCode(@PathVariable("code") String product) {
    return productService.find(product);
  }

}
