package simpleppob.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import simpleppob.dao.ProductDao;
import simpleppob.dto.ProductDto;

@Repository
public class ProductDaoImpl implements ProductDao {

  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Autowired
  public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
    this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
  }

  @Override
  public ProductDto findByProductCode(String productCode) {
    StringBuilder sql = new StringBuilder();
    sql.append(
        "SELECT T_MST_PRODUCT.PRICE, T_MST_PRODUCT.CODE, T_MST_SUPPLIER.CODE, T_MST_PRODUCT.TOKEN ")
        .append(
            "FROM T_MST_PRODUCT INNER JOIN T_MST_SUPPLIER ON T_MST_PRODUCT.SUPPLIER_ID = T_MST_SUPPLIER.ID ")
        .append(
            "WHERE T_MST_PRODUCT.CODE = :product_code ORDER BY T_MST_PRODUCT.PRICE ASC LIMIT 1");

    Map<String, String> params = new HashMap<>();
    params.put("product_code", productCode);

    try {
      return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, (rs, row) -> {
        ProductDto product = new ProductDto();
        product.setPrice(BigDecimal.valueOf(rs.getDouble(1)));
        product.setProductCode(rs.getString(2));
        product.setSupplierCode(rs.getString(3));
        product.setToken(rs.getString(4));
        return product;
      });
    } catch (EmptyResultDataAccessException empty) {
      throw empty;
    }
  }

}
