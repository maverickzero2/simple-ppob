package simpleppob.dao.impl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import simpleppob.dao.TransactionDetailDao;
import simpleppob.json.InquiryDataRequest;

@Repository
public class TransactionDetailDaoImpl implements TransactionDetailDao {

  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Autowired
  public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
    this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
  }

  @Override
  public int save(InquiryDataRequest inquiryData, String state) throws JsonProcessingException {
    StringBuilder sql = new StringBuilder();
    sql.append("INSERT INTO T_MST_TRANSACTION_DETAIL ")
        .append(
            "(TRANSACTION_CODE, STATE, PRODUCT_CODE, USER_EMAIL, NOMINAL, RAW_DATA, JOIN_TIME) ")
        .append(
            "VALUES (:transaction_code, :state, :product_code, :email, :nominal, :data, NOW())");

    Map<String, Object> params = new HashMap<>();
    params.put("transaction_code", inquiryData.getTransactionCode());
    params.put("state", state);
    params.put("product_code", inquiryData.getProductCode());
    params.put("email", inquiryData.getEmail());
    params.put("nominal", inquiryData.getNominal().doubleValue());
    params.put("data", new ObjectMapper().writeValueAsString(inquiryData));

    return namedParameterJdbcTemplate.update(sql.toString(), params);
  }

}
