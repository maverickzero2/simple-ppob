package simpleppob.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import simpleppob.dao.TransactionDao;
import simpleppob.dto.InquiryDto;
import simpleppob.dto.PaymentDto;
import simpleppob.dto.ProductDto;
import simpleppob.json.InquiryDataRequest;
import simpleppob.type.TransactionState;

@Repository
public class TransactionDaoImpl implements TransactionDao {

  private static final String TRANSACTION_CODE = "transaction_code";
  private static final String STATE = "state";
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Autowired
  public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
    this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
  }

  @Override
  public int save(InquiryDataRequest inquiryData, ProductDto product) {
    StringBuilder sql = new StringBuilder();
    sql.append("INSERT INTO T_MST_TRANSACTION ")
        .append(
            "(CODE, STATE, USER_EMAIL, SUPPLIER_CODE, PRODUCT_CODE, PRODUCT_TOKEN, FEE, TOTAL, JOIN_TIME, UPDATE_TIME) ")
        .append(
            "VALUES (:transaction_code, :state, :email, :supplier_code, :product_code, :token, :fee, :total, NOW(), NOW())");

    Map<String, Object> params = new HashMap<>();
    params.put(TRANSACTION_CODE, inquiryData.getTransactionCode());
    params.put(STATE, TransactionState.INQUIRY.toString());
    params.put("email", inquiryData.getEmail());
    params.put("supplier_code", product.getSupplierCode());
    params.put("product_code", inquiryData.getProductCode());
    params.put("token", product.getToken());
    params.put("fee", product.getPrice());

    double total = product.getPrice().doubleValue() + inquiryData.getNominal().doubleValue();
    params.put("total", BigDecimal.valueOf(total));

    try {
      return namedParameterJdbcTemplate.update(sql.toString(), params);
    } catch (DuplicateKeyException d) {
      throw d;
    }
  }

  @Override
  public InquiryDto findInquiry(String transactionCode) {
    StringBuilder sql = new StringBuilder();
    sql.append(
        "SELECT CODE, USER_EMAIL, SUPPLIER_CODE, PRODUCT_TOKEN, STATE, FEE, TOTAL, PRODUCT_CODE ")
        .append("FROM T_MST_TRANSACTION WHERE CODE = :transaction_code AND STATE = :state LIMIT 1");

    Map<String, String> params = new HashMap<>();
    params.put(TRANSACTION_CODE, transactionCode);
    params.put(STATE, TransactionState.INQUIRY.toString());

    try {
      return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, (rs, row) -> {
        InquiryDto inquiryDto = new InquiryDto();
        inquiryDto.setTransactionCode(rs.getString(1));
        inquiryDto.setEmail(rs.getString(2));
        inquiryDto.setSupplierCode(rs.getString(3));
        inquiryDto.setToken(rs.getString(4));
        inquiryDto.setState(rs.getString(5));
        inquiryDto.setFee(BigDecimal.valueOf(rs.getDouble(6)));
        inquiryDto.setTotal(BigDecimal.valueOf(rs.getDouble(7)));
        inquiryDto.setProductCode(rs.getString(8));

        return inquiryDto;
      });
    } catch (EmptyResultDataAccessException e) {
      throw e;
    }
  }

  @Override
  public int updateState(String transactionCode, String state) {
    String sql =
        "UPDATE T_MST_TRANSACTION SET STATE = :state WHERE STATE <> :state AND CODE = :code";

    Map<String, String> params = new HashMap<>();
    params.put(STATE, state);
    params.put("code", transactionCode);

    return namedParameterJdbcTemplate.update(sql, params);
  }

  @Override
  public PaymentDto findPayment(String transactionCode) {
    StringBuilder sql = new StringBuilder();
    sql.append(
        "SELECT T_MST_TRANSACTION.CODE, T_MST_TRANSACTION.USER_EMAIL, T_MST_TRANSACTION.SUPPLIER_CODE, T_MST_TRANSACTION.PRODUCT_TOKEN, T_MST_TRANSACTION.STATE, T_MST_TRANSACTION.FEE, T_MST_TRANSACTION.TOTAL, T_MST_TRANSACTION.PRODUCT_CODE, T_MST_USER.BALANCE ")
        .append(
            "FROM T_MST_TRANSACTION INNER JOIN T_MST_USER ON T_MST_TRANSACTION.USER_EMAIL = T_MST_USER.EMAIL ")
        .append(
            "WHERE T_MST_TRANSACTION.CODE = :transaction_code AND T_MST_TRANSACTION.STATE = :state LIMIT 1");

    Map<String, String> params = new HashMap<>();
    params.put(TRANSACTION_CODE, transactionCode);
    params.put(STATE, TransactionState.PAYMENT.toString());

    try {
      return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, (rs, row) -> {
        PaymentDto paymentDto = new PaymentDto();
        paymentDto.setTransactionCode(rs.getString(1));
        paymentDto.setEmail(rs.getString(2));
        paymentDto.setSupplierCode(rs.getString(3));
        paymentDto.setToken(rs.getString(4));
        paymentDto.setState(rs.getString(5));
        paymentDto.setFee(BigDecimal.valueOf(rs.getDouble(6)));
        paymentDto.setTotal(BigDecimal.valueOf(rs.getDouble(7)));
        paymentDto.setProductCode(rs.getString(8));
        paymentDto.setBalance(BigDecimal.valueOf(rs.getDouble(9)));

        return paymentDto;
      });
    } catch (EmptyResultDataAccessException e) {
      throw e;
    }
  }

  @Override
  public PaymentDto find(String transactionCode) {
    StringBuilder sql = new StringBuilder();
    sql.append(
        "SELECT CODE, USER_EMAIL, SUPPLIER_CODE, PRODUCT_TOKEN, STATE, FEE, TOTAL, PRODUCT_CODE ")
        .append("FROM T_MST_TRANSACTION WHERE CODE = :transaction_code LIMIT 1");

    Map<String, String> params = new HashMap<>();
    params.put(TRANSACTION_CODE, transactionCode);

    try {
      return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, (rs, row) -> {
        PaymentDto paymentDto = new PaymentDto();
        paymentDto.setTransactionCode(rs.getString(1));
        paymentDto.setEmail(rs.getString(2));
        paymentDto.setSupplierCode(rs.getString(3));
        paymentDto.setToken(rs.getString(4));
        paymentDto.setState(rs.getString(5));
        paymentDto.setFee(BigDecimal.valueOf(rs.getDouble(6)));
        paymentDto.setTotal(BigDecimal.valueOf(rs.getDouble(7)));
        paymentDto.setProductCode(rs.getString(8));

        return paymentDto;
      });
    } catch (EmptyResultDataAccessException e) {
      throw e;
    }
  }

}
