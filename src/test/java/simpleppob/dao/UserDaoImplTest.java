package simpleppob.dao;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import simpleppob.dao.impl.UserDaoImpl;
import simpleppob.dto.UserDto;
import simpleppob.json.SaveUserRequest;

public class UserDaoImplTest {

  private EmbeddedDatabase db;

  @Before
  public void setup() {
    db = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL)
        .addScript("db/sql/create.sql").addScript("db/sql/insert.sql").build();
  }

  @Test
  public void saveTest() {
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    SaveUserRequest user = new SaveUserRequest();
    user.setEmail("adetia");
    user.setPassword("adetest");

    int save = userDao.save(user);

    Assert.assertTrue(save > 0);
  }

  @Test(expected = DuplicateKeyException.class)
  public void saveEmailExistingTest() {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    SaveUserRequest user = new SaveUserRequest();
    user.setEmail("adetia");
    user.setPassword("adetest");

    userDao.save(user);
  }

  @Test
  public void findTest() {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    UserDto user = userDao.find("adetia");

    Assert.assertNotNull(user);
    Assert.assertEquals(0, user.getId());
    Assert.assertEquals("adetia", user.getEmail());
    Assert.assertEquals("adetest", user.getPassword());
    Assert.assertEquals(100000d, user.getBalance().doubleValue(), 0d);
  }

  @Test(expected = EmptyResultDataAccessException.class)
  public void findNotFoundTest() {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    userDao.find("adetia.marhadi");
  }

  @Test
  public void validatePasswordTest() {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    boolean valid = userDao.validatePassword("adetia", "adetest");

    Assert.assertTrue(valid);
  }

  @Test
  public void invalidatePasswordTest() {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    boolean valid = userDao.validatePassword("adetia", "adetest**");

    Assert.assertFalse(valid);
  }

  @Test
  public void updateBalanceTest() {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    int save = userDao.updateBalance("adetia", BigDecimal.valueOf(200));

    Assert.assertTrue(save > 0);
  }

  @Test
  public void findBalanceTest() throws Exception {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    BigDecimal balance = userDao.findBalance("adetia");

    Assert.assertTrue(balance.compareTo(BigDecimal.ZERO) > 0);
  }

  @Test
  public void findBalanceNotFoundTest() {
    saveTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    try {
      userDao.findBalance("adetiaaaa");
    } catch (Exception e) {
      Assert.assertTrue(true);
    }

  }

  @After
  public void done() {
    db.shutdown();
  }

}
